Jump games for the TI-83+/TI-84+ Graphing Calculator

Program converted from 8xp to text at https://www.cemetech.net/sc/

Downloads: https://bitbucket.org/drazil100/jump-series-ti-84-plus/downloads/
