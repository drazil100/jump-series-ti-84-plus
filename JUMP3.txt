ClrHome
Disp " )------------("
Disp "(    JUMP 3    )"
Disp " )------------("
Disp " The final jump"
Disp " "
Disp " [Programed by]"
Disp "  Austin Allman"
Pause 
ClrHome
AxesOff
ClrDraw
~2.9375->Xmin
2.9375->Xmax
~1.9375->Ymin
1.9375->Ymax
46->A
2->B
~1->C
~1->D
~1->E
~1->F
~1->G
~1->H
~1->I
46->J
0->K
1->V
1->L
1->S
~1->T
0->X
0->theta
100->Z
Disp " ","> NORMAL"," ","  HARD"
Lbl 4
getKey->theta
If theta=25 and L=2
Then
	ClrHome
	Disp " ","> NORMAL"," ","  HARD"
	1->L
End
If theta=34 and L=1
Then
	ClrHome
	Disp " ","  NORMAL"," ","> HARD"
	2->L
End
If theta=45
Then
	60->X
	Goto 3
End
If theta!=105
Goto 4
0->theta
{~3,~2,~1,0,1,2,3}->L1
Lbl 1
{C,D,E,F,G,H,I}->L2
L2+2->L3
Plot1(xyLine,L1,L2,plotdot)
Plot2(xyLine,L1,L3,plotdot)
Pxl-On(A,B)
Pxl-On(J,K)
If X=30-L or X=56
Then
	Line(2,1.25,2,.25)
	Line(2,~.25,2,~1.25)
	Line(2,~2,2,H+.5)
	Line(2,H+1.5,2,2)
End
If X=30 or X=58
Then
	Line(1,1.25,1,.25)
	Line(1,~.25,1,~1.25)
	Line(1,~2,1,G+.5)
	Line(1,G+1.5,1,2)
End
If X=31 or X=32 or X=60
Then
	Line(0,1.25,0,.25)
	Line(0,~.25,0,~1.25)
	Line(0,~2,0,F+.5)
	Line(0,F+1.5,0,2)
End
If X=34
Then
	Line(~1,1.25,~1,.25)
	Line(~1,~.25,~1,~1.25)
	Line(~1,~2,~1,E+.5)
	Line(~1,E+1.5,~1,2)
End
If X=36
Then
	Line(~2,1.25,~2,.25)
	Line(~2,~.25,~2,~1.25)
	Line(~2,~2,~2,D+.5)
	Line(~2,D+1.5,~2,2)
End
D->C
E->D
F->E
G->F
H->G
I->H
randInt(2*T,~1*T)->theta
If X>30-L
randInt(4*T,~2*T)->theta
If theta=~1*T or theta=~2*T
randInt(2*T,~2*T)->theta
If H=0
randInt(0,~2*T)->theta
If H=~2
randInt(0,2*T)->theta
If H=1
randInt(0,~3)->theta
If H=.5
randInt(2,~2)->theta
If H=~3
randInt(3,0)->theta
If H=~2.5
randInt(2,~2)->theta
theta*.5->theta
H+theta->I
If I>1
Then
	~1->T
	1->I
End
If I<~3
Then
	1->T
	~3->I
End
Lbl 2
If K>=B-(2/L) and K<=B+(2/L) and J>=A-(2/L) and J<=A+(2/L)
V*~1->V
If K<B-(2/L) or K>B+(2/L) or J>A+(2/L) or J<A-(2/L)
1->V
If theta=21 and B=K and J+1=A
~1->V
If theta=21 and B=K and J-1=A
~1->V
getKey->theta
If theta=105
Then
	Pause 
	Goto 2
End
If theta=45 or X=31 or X=60 or A=0 or A=62 or B=0
Goto 3
If pxl-Test(A-1,B)=1 and S=1
Then
	~100->Z
	~1->S
End
If pxl-Test(A+1,B)=1 and S=~1
Then
	100->Z
	1->S
End
If A>Z and pxl-Test(A-1,B)!=1
Then
	Pxl-Off(A,B)
	A-1->A
	Pxl-On(A,B)
End
If A<Z and pxl-Test(A+1,B)!=1
Then
	Pxl-Off(A,B)
	A+1->A
	Pxl-On(A,B)
End
If theta=26 and pxl-Test(A,B+1)!=1
Then
	Pxl-Off(A,B)
	B+1->B
	Pxl-On(A,B)
End
If theta=24 and pxl-Test(A,B-1)!=1 and B!=2
Then
	Pxl-Off(A,B)
	B-1->B
	Pxl-On(A,B)
End
If A=0 or A=62 or B=0
Goto 3
If theta=21 and pxl-Test(A+S,B)=1
A-8S->Z
If A=Z
100S->Z
If V=~1
Goto 2
If pxl-Test(A,B+1)=1 and A=J and K+1=B
Goto 3
If pxl-Test(A,B-1)=1 and A=J and K-1=B
Goto 3
If K+1=B and A=J
Then
	Pxl-Off(A,B)
	B+1->B
	Pxl-On(A,B)
End
If K-1=B and A=J
Then
	Pxl-Off(A,B)
	B-1->B
	Pxl-On(A,B)
End
If K>B
Then
	Pxl-Off(J,K)
	K-1->K
	Pxl-On(J,K)
End
If K<B
Then
	Pxl-Off(J,K)
	K+1->K
	Pxl-On(J,K)
End
If pxl-Test(A+1,B)=1 and B=K and J+1=A
Goto 3
If pxl-Test(A-1,B)=1 and B=K and J-1=A
Goto 3
If J+1=A and B=K
Then
	Pxl-Off(A,B)
	A+1->A
	Pxl-On(A,B)
End
If J-1=A and B=K
Then
	Pxl-Off(A,B)
	A-1->A
	Pxl-On(A,B)
End
If J>A
Then
	Pxl-Off(J,K)
	J-1->J
	Pxl-On(J,K)
End
If J<A
Then
	Pxl-Off(J,K)
	J+1->J
	Pxl-On(J,K)
End
If B>62
Then
	B-16->B
	K-16->K
	X+L->X
	If K<1
	1->K
	Goto 1
End
Goto 2
Lbl 3
PlotsOff 
Zoom Out
ClrDraw
ClrHome
AxesOn
If X!=31 and X!=60
Then
	Disp "GAME OVER"
	Pause 
End
0->A
0->B
0->C
0->D
0->E
0->F
0->G
0->H
0->I
0->J
0->K
0->L
0->S
0->T
0->V
0->Z
0->X
0->theta
ClrList L1,L2,L3
ClrHome
